#Rational Drowning
##Definition
Provide a series of data-driven insights that highlight the audience's pain points and challenges. Use facts to show them the potential pitfalls or missed opportunities in their current approach.
##Prompt ChatGPT
Creating a "Rational Drowning" based on the Challenger Sale methodology involves presenting the audience with a series of insights or data points that make them realize the gravity of their current situation or challenges. To guide the creation of an effective Rational Drowning, provide:

- **Topic or Issue**: Define the main issue or challenge you're addressing, so the focus is clear.

- **Target Audience**: Describe the specific group or individual you're addressing to ensure relevancy.

- **Key Data Points & Insights**: Provide (or request) specific statistics, trends, facts, or figures that underscore the depth of the problem or issue.

- **Desired Realization**: What should the audience realize or feel after encountering this data? It helps to set a clear objective, whether it's a sense of urgency, recognition of a gap, or the magnitude of a missed opportunity.

- **Tone & Style**: Specify if you want the presentation of data to be stark, analytical, compelling, or in any other particular style.

##Example from ChatGPT
For instance, a prompt might be:

<code>Craft a Rational Drowning for e-commerce businesses that overlook mobile users. Include data on the rapidly increasing mobile shopping trends, cart abandonment rates on non-mobile-friendly sites, and the potential revenue lost. The goal is for these businesses to recognize the critical importance of optimizing for mobile. The tone should be analytical yet compelling.</code>

Structuring your prompt in this manner ensures that the Rational Drowning piece effectively communicates the gravity of the situation to the intended audience.

###Output
>*In the swiftly evolving landscape of online shopping, there's a trend that's impossible to ignore: the meteoric rise of mobile users. Recent data uncovers that a staggering 65% of all e-commerce traffic now originates from mobile devices[^1], a figure that's only predicted to surge in the coming years. Yet, herein lies the dilemma: non-mobile-optimized websites are witnessing cart abandonment rates as high as 85%[^2].*

>*Pause for a moment and consider the ramifications. For every 100 potential customers, 85 might be walking away, their virtual carts filled but never checked out. The revenue implications are profound. Businesses that are not mobile-optimized are potentially forfeiting millions, if not billions, in lost sales annually[^3].*

>*These aren't mere numbers; they are indicative of a profound shift in consumer behavior. The question isn't whether mobile shopping is essential—it's how much longer can any e-commerce entity afford to overlook it without severely compromising their growth and profitability?*

1. *Mobile E-commerce Stats*, Mobile Trends Report 2023
2. *Shopping Cart Abandonment Research*, E-commerce Insights, 2022
3. *Lost Revenue Analysis*, Digital Market Review, 2023


##Best Practice
##Practical Application
