#**Experiment Use Case Selection Workshop**
##Background
The Leaders' Intent Alignment Workshop is a 120 minute workshop to facilitate a dialogue that combines best of breed industry practices with the unique opportunities and challenges presented within your organisation.  

The workshop consists of two 60 minute executive-to-executive meetings to gather relevant context, inspire new ways of thinking, and build rapport.  We aim to provide greater clarity to strategy development based on deep experience in industry and InnerSource evolutions.  Ultimately, we will co-create a clear and concise leader’s intent, which will provide alignment for follow-up workshops.

##Attendees
+ Sponsor:  *Executive with direct reporting line to all three anchors*
+ Facilitator:  *Coach or Consultant outside of the reporting lines or external*
+ Anchor Owners:  *A representative from each anchor who can own decisions*
+ Scribe (optional):  *Graphic notetaker or sketch-noter*

##Agenda
###**First Session: Organisational Topography**
+ Introduction
+ Review:  Inverting the Pyramid and the dynamic between Strategy and Implementation
+ Understanding Higher Intent
    + Situation / Background
    + Two Levels Up:  What and Why
    + One Level Up:  Task(s) and Purpose
+ Identifying Key Terrain

###**Second Session: The Art of Action**
+ Introduction
+ Review:  The ⅓ to ⅔ Rule and Direction vs Management/Leadership
+ Leading Through Intent
    + My Intent
    + Implied Tasks
    + Boundaries
+ Assumption:  A Double Edged Sword

##Output
A one page artifact that will be used to align strategy developers and implementation planners

##Desired Outcome
Leadership alignment and direction that clearly and concisely convey strategy intent, which will inform implementation planning without determining the overall solution.

##Source Material
|![type:image](https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1387302434i/9973202.jpg)|![type:image](https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1328859803i/3621358.jpg)  |![type:image](https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1433739086i/33313.jpg)  |
|:---:|---|---|

+ [The Art of Action](https://www.goodreads.com/book/show/9973202-the-art-of-action)
+ [Inverting the Pyramid: The History of Football Tactics](https://www.goodreads.com/book/show/3621358-inverting-the-pyramid)
+ [Kitchen Confidential](https://www.goodreads.com/book/show/33313.Kitchen_Confidential)
