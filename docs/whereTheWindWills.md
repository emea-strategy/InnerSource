#**Where the Wind Wills**
For the spider, once attached to her anchor, she lowers herself an appropriate distance and waits for a slight breeze to bring her into contact with an adjoining solid surface.  It is important to note that she doesn't attempt to force the connection, but rather follows where the wind wills, or the path of least resistance.  While the desired outcome from any InnerSource journey should be to drive advantage for your organisation, that advantage can be derived from many different areas once connecting anchor points have been achieved.

##Three Anchors for a Coalition of the Willing
Getting started is often the greatest inhibitor to progress within InnerSource journeys.  Planning yields diminished return over time, and early in the journey can cripple much needed innovation.  On the other hand, without any planning, an incoming gale could blow the first thread irrevocably awry.  Our advice, start with two differentiated use cases that promise high impact within the organisation, but both should be achievable with medium to low levels of complexity.  

![Experiment Use Case Selection](https://emea-strategy.gitlab.io/InnerSource/assets/images/Experiment Use Case.png "Painkillers over Vitamins")

These **three anchors**--our origin and two teams tied to use cases--create an initial grouping of like minded individuals, or the start to enlisting an army of volunteers for our experiment.  During initial use case assessment, use the list below to help triage potential candidates.

|**Embrace**|**Avoid**|
|---|---|
|Painkillers|Vitamins|
|Low Cognitive Load|Complicated|
|Access to End Users|Fishbowl Requirements|
|Top Line Generative|Diminished Return|
|Org Wide Impact|Low Hanging Fruit|

##**Practical Exercise:** [Experiment Use Case Selection](/experimentUseCaseSelectionWorkshop)
