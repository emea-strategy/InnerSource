#Emotional Impact
##Definition
Connect the challenges and insights discussed to their personal and emotional implications. This deepens the urgency and makes the problem more tangible, leading the audience to see the need for a change.
##Prompt ChatGPT
The "Emotional Impact" stage, based on the Challenger Sale methodology, is designed to resonate with the audience's feelings, creating a connection between the challenges or issues at hand and the personal and emotional consequences they may face. To effectively guide the crafting of an Emotional Impact piece, provide the following information:

- **Topic or Issue**: Clearly describe the main issue or challenge you're addressing, so the emotional implications can be correctly tied to it.

- **Target Audience**: Detail the specific group or individuals you're addressing to ensure the content resonates deeply with their personal and professional experiences.

- **Desired Emotional Response**: What emotion do you want to evoke? Concern? Empathy? Hope? Determination? This helps tailor the content to elicit the intended feeling.

- **Real-life Consequences**: If you have specific scenarios or stories that highlight the emotional toll of the issue, share them. Real anecdotes or hypothetical situations can be powerful tools to create an emotional connection.

- **Tone & Style**: Should the Emotional Impact be dramatic, compassionate, urgent, or introspective? The manner in which you convey emotions can determine how they're received.

##Example from ChatGPT
For example, a prompt might be:

<code>Write an Emotional Impact piece for nonprofit leaders about the consequences of insufficient funding for children's education programs. Evoke a sense of urgency and concern, highlighting the potential long-term effects on the children's futures. Use a compassionate and poignant tone.</code>

By structuring your prompt in this manner, you ensure that the Emotional Impact piece is both compelling and deeply resonant with your intended audience.
###Output
>*Imagine a bright-eyed child, eager to learn, stepping into a classroom only to find it lacking the basic resources to foster their growth. This is the stark reality for countless children when educational programs remain underfunded. Each dollar that doesn't find its way to these initiatives isn't just a statistic—it's a lost opportunity, a dream deferred.*

>*For many of these children, education is the beacon of hope, the singular pathway to breaking cycles of poverty and realizing their full potential. When we underfund these programs, we aren't just limiting their academic achievements; we're dimming the light on their dreams, aspirations, and futures. We're silently conveying that their potential isn't worth the investment.*

>*Every moment of hesitation, every funding delay, casts a long shadow on these young lives. The choices we make today echo in the lives of these children and the communities they represent. Let us feel the weight of this responsibility, not as a burden, but as a heartfelt call to action. For in their success, in their dreams realized, lies the future we all hope to see.*

##Best Practice
##Practical Application
