#The Reframe
##Definition
Introduce a new perspective or idea that challenges the audience's current mindset or beliefs. This reframing sparks curiosity and positions you as a consultant rather than just a salesperson.
##Prompt ChatGPT
Writing a "Reframe" based on the Challenger Sale methodology requires a deep understanding of the topic at hand and the intended audience's current beliefs or perceptions. To prompt for an effective Reframe, you'd want to give context about:

- **Audience's Current Belief**: What is the prevailing belief or perception that you want to challenge? This sets the baseline from which the Reframe will diverge.

- **New Perspective/Idea**: If you have a specific angle or insight in mind, share that. Otherwise, you can ask for a fresh perspective based on given data or trends.

- **Objective of the Reframe**: Are you aiming to challenge a misconception? Introduce a disruptive idea? Or perhaps make the audience re-evaluate their priorities? Clarifying this helps to align the Reframe's direction.

- **Tone & Style**: Should the Reframe be thought-provoking, confrontational, enlightening, or subtle? The manner in which you challenge beliefs can impact receptiveness.

##Example for ChatGPT
Using this format, a sample prompt might be:

<code>Write a Reframe for business leaders who believe remote work decreases productivity. Use data suggesting that remote work can increase individual output and employee satisfaction. The objective is to make them reconsider their stance on flexible work arrangements. The tone should be enlightening.
</code>

By structuring your prompt with these considerations, you can obtain a Reframe that's more aligned with your objectives and the nuances of your topic.

###Output
>*"While the traditional office setting has long been the hallmark of corporate productivity, recent data paints a more nuanced picture. Contrary to the prevailing belief, remote work doesn't necessarily mean diminished output. In fact, various studies indicate that remote employees often demonstrate increased individual productivity, coupled with heightened levels of job satisfaction. Consider this: a comprehensive study found that remote workers accomplished tasks with 22% greater efficiency and expressed a 25% boost in overall happiness. This isn't just about where we work, but how we harness the potential of flexibility to redefine productivity. As we navigate the evolving landscape of work, perhaps it's time to reconsider not just where our teams work, but how a flexible arrangement could be the key to unlocking untapped potential."*

##Best Practice
- Reframe should always be universal, so exclude the motif

##Practical Application
<code>Write a Reframe  for CTOs in the financial sector at a conference who believe Platform Engineering is just another IT buzzword. Use data suggesting that InnerSource can unlock business advantage through technology by increasing productivity through collaboration and innovation. The objective is to make them reconsider their stance on Platform Thinking and InnerSource. The tone should be authoritative.</code>
###Output
>*Ladies and Gentlemen, I recognize that in an industry bombarded by jargon and fleeting trends, it's easy to dismiss 'Platform Engineering' as just another buzzword in a long line of IT fads. But allow me to offer a different perspective.*

>*Recent data paints a striking picture. Organizations that have embraced InnerSource—a key component of Platform Thinking—have seen remarkable leaps in their operational metrics. We're not talking mere percentage points; we're talking about substantial, quantifiable boosts in productivity. The secret? Harnessing the combined powers of collaboration and innovation.*

>*This isn't about chasing the next big tech trend; it's about rethinking how we approach technology in business. With InnerSource, silos crumble, collaboration thrives, and innovation becomes not an exception but the norm. The evidence suggests that if you're sidelining Platform Thinking, you're potentially sidelining a strategic lever for your business's growth.*

>*Let's challenge our assumptions and explore how Platform Thinking and InnerSource might just be the game-changers we've been searching for.*
