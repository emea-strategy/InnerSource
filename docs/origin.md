#**Origin**
Every journey requires a starting point, and for a spider it's a place to anchor her first silk thread.  Very few anchor points are the same, just as very few organisations start from the same waypoint along the journey.  We have found it is not *where you start*, but **how you start**, that is vitally important.

In [Leading Change](https://www.kotterinc.com/methodology/8-steps/), by John Kotter, he describes the first three necessary stages of transformation:
  + Create Sense of Urgency
  + Build a Guiding Coalition
  + Form a Strategic Vision

Smelted together with [The Art of Action](https://www.stephenbungay.com/Books.ink), by Stephen Bungay, these two guiding texts form the perfect mix of theory and practice.  It is important to take time upfront to develop vision and strategy from an executive level down, or else you run the risk of poorly setting expectations.

![Leading Change](https://kotterintl2.wpenginepowered.com/wp-content/uploads/2022/04/8Steps_Final_Transparent.png "The Big Opportunity")



##**Practical Exercise:** [Leaders Intent Aligntment](/leadersIntentAlignment)
