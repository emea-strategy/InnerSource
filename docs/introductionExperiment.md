#Lean Startup Approach to Influence
##Variables
- Audience: *CTOs attending a conference keynote*
- Motif: *Cyberpunk*
- Tone:  *Authoritative*
- Key Insights: *Importance of collaboration, innovation, and productivity to Platform Engineering*
- Goal of Warmer:  *Concerned about Analysis Paralysis*
- Current Belief:  *Platform Engineering is just another IT buzzword*
- New Perspective/Idea:  *unlock business advantage through technology by increasing productivity through collaboration and innovation*
- Objective of Reframe:
- Topic or Issue:
- Key Data Points & Insights:
- Desired Realization:
- Desired Emotional Response:
- Real-life Consequences:
- Background information:
- Current Belief or Practice:  *Community, collaboration, and technology*
- Desired Outcome:  *Transformative potential in reshaping business processes through InnerSource*
- Unique Value Proposition:  *If we build it, they won't come.  If they build it, they will love it!*
