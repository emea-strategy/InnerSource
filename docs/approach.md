#**Welcome**
Have you ever watched a spider spin her web?  Aside from the mesmerizing experience, the keen observer will begin to glean emergent patterns in the process.  As the spider traps her prey, instead of being destroyed, the web is made more resilient and reinforced with each undertaking.

![Spiderwebs are elegant](https://emea-strategy.gitlab.io/InnerSource/assets/images/spiderweb.png "InnerSource as the Spiderweb")

We find that nature provides excellent examples of elegant systems and their patterns, so we have fashioned our approach to InnerSource from the spider's web.  Although complex and sophisticated in later stages, it's important to know that construction of every web starts the same way.
