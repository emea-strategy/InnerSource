#**Mapping**
In Simon Wardley’s [Crossing the River by Feeling the Stones](https://vimeo.com/189984496) presentation, he discusses leadership strategy through the lens of the exceedingly rare context specific situational awareness and the far more common, “alchemy, gut feel, and whatever is trending in the HBR,” strategy employed in most board rooms.  We focus on the former.

It is critical to first draw a map for situational awareness, otherwise we run the risk of speaking past each other and cannot anchor our understanding of the problem in reality.  While we frequently use different map mediums ([Wardley](https://training.dddeurope.com/wardley-mapping-ben-mosior-jabe-bloom/), [Value Stream](https://openpracticelibrary.com/practice/vsm-and-mbpm/), [Domain Storytelling](https://openpracticelibrary.com/practice/domain-storytelling/)), the overriding intent is to iteratively reduce the known uncertainty of the problem space by understanding the organisational topography and determining key terrain that is essential to success.  

Once we have the beginning of a map, we can more easily orient to an incremental goal and draw a line of bearing.  The next step is to anticipate obstacles and develop a course of action.  Working rapidly within the team, we can wargame multiple scenarios to determine the best path, conscious that planning has a diminishing return.  

In addition, the team will have to make assumptions to maintain planning momentum, which will be subsequently validated as either facts, known gaps, constraints/limitations, or risks.

The final step before embarking on this adventure together is to establish **lines of communication**.  Teams will need to be empowered to make decisions and implement changes at the level where the problems manifest, as long as those decisions are in line with leaders’ intent and the organisational guiding principles.  At this stage, we will codify, define, and proliferate intent and guiding principles to avoid confusion throughout the journey.  

Additionally, we will establish a model whereby contribution to the solution grants ownership of the overall roadmap to attract early adopters, which will allow for asynchronous collaboration and communication.
